const express = require('express');
const authController = require('../controllers/auth');
const router = express.Router();

router.post('/registrar', authController.registrar);
router.post('/login', authController.login);
router.get('/cerrar', authController.cerrar);
router.get('/prestamos', authController.prestamos);
router.get('/aprobar/:id', authController.aprobar);
module.exports = router;
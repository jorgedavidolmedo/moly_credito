const express = require('express');
const router = express.Router();
const checkAuth = require('../middelware/check-auth');
const pagesController = require('../controllers/pages');

router.get('/', checkAuth, (req, res) => {
    res.render('main', { layout: 'main' });
});

router.get("/registrar", (req, res) => {
    res.render('main', { layout: 'usuarios/registrar' });
});

router.get("/login", (req, res) => {
    res.render('main', { layout: 'usuarios/login' });
});

router.get("/pendientes", (req, res) => {
    res.render('main', { layout: 'usuarios/pendientes' });
});

router.get("/consultar/:desde/:hasta", pagesController.consultarPanel);

module.exports = router;
const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
    try {
        const decoded = jwt.verify(req.cookies.AuthToken, process.env.JWT_SECRET);
        req.userData = decoded;
        next();
    } catch (error) {
        return res.status(401).render('main', {
            layout: 'usuarios/login',
            message: 'Debes iniciar session'
        });
    }


}
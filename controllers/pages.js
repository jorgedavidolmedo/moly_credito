const { query } = require("express");
const mysql = require("mysql");
const db = mysql.createConnection({
    host: process.env.DATABASE_HOST,
    user: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE
});

exports.consultarPanel = (req, res) => {
    let array = [];
    console.log(req.params.desde);
    var query = `SELECT a.idprestamo as id,
                 SUM(a.montoneto+a.montiiva) as total,
                 b.descripcion as descripcion,
                 b.idtipoprestamo as id_tipo
                 FROM prestamo a INNER JOIN tipoprestamo b ON a.idtipoprestamo=b.idtipoprestamo
                 WHERE a.estado=1 AND 
                 a.fecha>=? AND 
                 a.fecha<=? AND 
                 b.idtipoprestamo IN (1,2,3,6) AND 
                 a.montoneto+a.montiiva>0 
                 GROUP BY a.idtipoprestamo
                 ORDER BY a.idtipoprestamo`;
    db.query(query, [req.params.desde, req.params.hasta], async(error, results) => {
        if(error){
            console.log(error);
            return;
        }

        res.json({
            resultado:results
        })          
    });
};
const mysql = require("mysql");
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const logout = require('express-passport-logout');

const db = mysql.createConnection({
    host: process.env.DATABASE_HOST,
    user: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE
});

exports.login = async(req, res) => {
    try {
        const { email, password } = req.body;
        if (!email || !password) {
            return res.status(400).render('main', {
                layout: 'usuarios/login',
                message: 'Debes ingresar el nombre y el password'
            });
        }

        db.query("SELECT * FROM usuario WHERE nombre= ?", [email], async(error, results) => {

            console.log("#######PASSWORD 2#########");
            console.log(results[0].password2);
            console.log("################");
            if (!results || !(await bcrypt.compare(password, results[0].password2))) {
                res.status(400).render('main', {
                    layout: 'usuarios/login',
                    message: 'El email o password no son válidos'
                });
            } else {
                
                const id = results[0].id;
                const token = jwt.sign({ id: id }, process.env.JWT_SECRET, {
                    expiresIn: process.env.JWT_EXPIRES_IN
                });

                console.log("TOKEN: " + token);
                const cookieOptions = {
                    expires: new Date(
                        Date.now() + process.env.JWT_COOKIE_EXPIRES * 24 * 60 * 60 * 1000
                    ),
                    httpOnly: true
                }
                res.cookie('AuthToken', token, cookieOptions);
                res.status(200).render('main', {
                    layout: 'main',
                    message: 'ok'
                });
            }
        });
    } catch (error) {
        console.log('###########');
        console.log(error);
        console.log('###########');
    }
};

exports.registrar = (req, res) => {

    const { usuario, direccion, contacto, perfil, email, password } = req.body;
    db.query('SELECT email FROM usuarios WHERE email = ? ', [email], async(error, results) => {
        if (error) {
            console.log(error);
        }

        if (results.length > 0) {
            return res.render('main', {
                layout: 'registrar',
                message: 'Este email ya existe.'
            });
        }

        let hashedPassword = await bcrypt.hash(password, 10);
        console.log(hashedPassword);

        db.query('INSERT INTO usuarios SET ?', {
            id_empresa: 1,
            descripcion: usuario,
            email: email,
            password: hashedPassword,
            foto: 'foto.jpg',
            telefono: 12,
            perfil: perfil,
            direccion: direccion,
            estado: 1
        }, (error, results) => {

            if (error) {
                console.log(error);
            } else {
                return res.render('main', {
                    layout: 'registrar',
                    message: 'El usuario se registro correctamente'
                });
            }

        });

    });

}

exports.cerrar = (req, res) => {
    res.clearCookie('AuthToken');
    res.status(400).redirect('/');
};

exports.prestamos = (req, res) => {
    var query = `SELECT a.idsolicitudcredito as id,
                 b.descripcion as tipo,
                 a.monto as monto,
                 a.garantia as garantia,
                 a.fecha as fecha
                 FROM solicitudcredito a INNER JOIN tipoprestamo b ON a.idtipoprestamo=b.idtipoprestamo
                 WHERE a.operarfueradelinea = 1 AND 
                 a.usuariofueralinea IS NULL ORDER BY a.idsolicitudcredito`;    
    db.query(query, async(error, results) => {
            res.json({
                prestamos:results
            })
    });

};


exports.aprobar = (req, res) => {
    var query = `UPDATE solicitudcredito SET fechafueralinea=now(), usuariofueralinea='WEB'
                 WHERE idsolicitudcredito = ?`;    
    db.query(query, [req.params.id], async(error, results) => {
            res.json({
                mensaje:"ok"
            })
    });

};
/**
 * Created by jorge on 06/04/2021.
 */
var app = angular.module('contalapp', ['ui.materialize']);

app.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('{[{');
    $interpolateProvider.endSymbol('}]}');
});

app.filter('myDateFormat', function myDateFormat($filter){
    return function(text){

        if(text==null){
            return true;
        }
        else
        {
            text = text.substring(0, 10);
            var fecha = text;
            fecha=fecha.split("-");
            fecha=fecha[2]+'/'+fecha[1]+'/'+fecha[0];
            return fecha;
        }


    }
});

app.filter('startFrom', function () {
    return function (input, start) {
        if (input) {
            start = +start;
            return input.slice(start);
        }
        return [];
    };
}).constant("kConstant",{
    "url":"http://localhost:3001/",
    "base":"/molycredito/"

}).service('usuariosByTerm',function($http,kConstant,$rootScope){
    return {
        async:function(term){
            return $http.get(kConstant.url+"/usuarios/getEntityUsuarioAllByTerm/"+term);
        }
    }
});
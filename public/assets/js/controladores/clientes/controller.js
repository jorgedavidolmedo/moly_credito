

var app = angular.module('contalapp');

app.controller('entityIndex',function ($scope,kConstant,$http,$window,$filter,$timeout) {


    String.prototype.replaceAll = function(str1, str2, ignore)
    {
        return this.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g,"\\$&"),(ignore?"gi":"g")),(typeof(str2)=="string")?str2.replace(/\$/g,"$$$$"):str2);
    }

    $scope.onPrecioKeyPress=function(keyEvent) {

        if (keyEvent.which === 13) {
            $scope.clientesAll();
        }
    }

   $scope.salir = function(){
    $window.location.href = kConstant.url+"clientes";
   }

  $scope.agregar = function(){
    $window.location.href = kConstant.url+"clientes/registrar";
  }

   $scope.entidad = {
       id:0,
       id_empresa:1,
       descripcion:null,
       documento:null,
       direccion:null,
       contacto:null,
       estado:1
   }

   $scope.guardar = function(){
       if($scope.entidad.descripcion==null || $scope.entidad.descripcion==""){
           alert("Debes agregar la descripción.");
           return;
       }

       if($scope.entidad.documento==null || $scope.entidad.documento==""){
            alert("Debes agregar el documento.");
            return;
        }

        if($scope.entidad.direccion==null || $scope.entidad.direccion==""){
            alert("Debes agregar la dirección.");
            return;
        }

        $http.post(kConstant.url+"personas/guardar/",$scope.entidad).
            then(function(response){
                $scope.salir();
         });
   }

   $scope.buscar = null;
   $scope.clientes = [];
   $scope.clientesAll = function(){
    var term = null;
    if($scope.buscar==null || $scope.buscar==""){
        term=0;
    }else{
        term = $scope.buscar;
    }
    console.log(kConstant.url+"personas/findAll/"+term);
    $http.get(kConstant.url+"personas/findAll/"+term).
        then(function(response){
            $scope.clientes = response.data.personas;    
    });
   }
   $scope.clientesAll(); 
   
   

});

function format(input)
{
    var num = input.value.replace(/\./g,'');
    if(!isNaN(num)){
        num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
        num = num.split('').reverse().join('').replace(/^[\.]/,'');
        input.value = num;
    }
    else{
        //console.log("NAN: "+num);
        //input.value = input.value.replace(/[^\d\.]*/g,'');
    }
}



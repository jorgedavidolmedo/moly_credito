

var app = angular.module('contalapp');

app.controller('entityIndex',function ($scope,kConstant,$http,$window,$filter,$timeout) {


    String.prototype.replaceAll = function(str1, str2, ignore)
    {
        return this.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g,"\\$&"),(ignore?"gi":"g")),(typeof(str2)=="string")?str2.replace(/\$/g,"$$$$"):str2);
    }


    $scope.formatDate = function(date) {

        var d = new Date(date || Date.now()),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [day, month, year].join('/');
    }

    $scope.pp = 0;
    $scope.dd = 0;
    $scope.pcp = 0;
    $scope.teso = 0;

    var date = new Date();
    var primerDia = new Date(date.getFullYear(), date.getMonth(), 1);
    var ultimoDia = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    $('#desde').val($scope.formatDate(primerDia));
    $('#hasta').val($scope.formatDate(ultimoDia));

   $scope.home = [];
   $scope.consultar = function(){
        var desde = $('#desde').val();
        desde = desde.split("/");
        desde = desde[2] + '-' + desde[1] + '-' + desde[0];

        var hasta = $('#hasta').val();
        hasta = hasta.split("/");
        hasta = hasta[2] + '-' + hasta[1] + '-' + hasta[0];

        //console.log(kConstant.url+"consultar/"+desde+"/"+hasta);
        $http.get(kConstant.url+"consultar/"+desde+"/"+hasta).
        //$http.get(kConstant.url+"consultar/2021-04-01/2021-04-31").
            then(function(response){
            console.log(response.data.resultado);
            if(response.data.resultado.length<=0){
                return;
            }
            $scope.pp = response.data.resultado[0].total;
            $scope.dd = response.data.resultado[1].total;
            $scope.pcp = response.data.resultado[2].total;
            $scope.teso = response.data.resultado[3].total;
        });
   }
   $scope.consultar(); 
   

});

function format(input)
{
    var num = input.value.replace(/\./g,'');
    if(!isNaN(num)){
        num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
        num = num.split('').reverse().join('').replace(/^[\.]/,'');
        input.value = num;
    }
    else{
        //console.log("NAN: "+num);
        //input.value = input.value.replace(/[^\d\.]*/g,'');
    }
}



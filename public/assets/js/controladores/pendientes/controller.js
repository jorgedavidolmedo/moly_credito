

var app = angular.module('contalapp');

app.controller('entityIndex',function ($scope,kConstant,$http,$window,$filter,$timeout) {



    String.prototype.replaceAll = function(str1, str2, ignore)
    {
        return this.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g,"\\$&"),(ignore?"gi":"g")),(typeof(str2)=="string")?str2.replace(/\$/g,"$$$$"):str2);
    }

    $scope.onPrecioKeyPress=function(keyEvent) {

        if (keyEvent.which === 13) {
            $scope.clientesAll();
        }
    }

   $scope.salir = function(){
    $window.location.href = kConstant.url+"clientes";
   }

  $scope.agregar = function(){
    $window.location.href = kConstant.url+"clientes/registrar";
  }

  $scope.aprobar = function(id){
    swal({
        title: "Deseas aprobar la solicitud # " + id + "?",
        text: "Atención!!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si, aprobar!",
        closeOnConfirm: true
    }, function() {
        $http.get(kConstant.url+"auth/aprobar/"+id).
        then(function(response){
            console.log(response.data.mensaje);
            //swal("Aprobar!", "La solicitud se aprobo correctamente.", "success");
            alert("La solicitud se aprobo correctamente.");
            /*$timeout(function() {
              
            }, 2000);*/
            $scope.getPrestamos();
        });
        
    });

  }


   $scope.buscar = null;
   $scope.prestamos = [];
   $scope.getPrestamos = function(){
    console.log(kConstant.url+"auth/prestamos");
    $http.get(kConstant.url+"auth/prestamos").
        then(function(response){
            console.log(response.data.prestamos);
            $scope.prestamos = response.data.prestamos;
    });
   }
   $scope.getPrestamos(); 
   
   

});

function format(input)
{
    var num = input.value.replace(/\./g,'');
    if(!isNaN(num)){
        num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
        num = num.split('').reverse().join('').replace(/^[\.]/,'');
        input.value = num;
    }
    else{
        //console.log("NAN: "+num);
        //input.value = input.value.replace(/[^\d\.]*/g,'');
    }
}


